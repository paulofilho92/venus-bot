const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const sql = require("sqlite");
const misc = require("./misc-functions.js");

sql.open("../botdb/astrologia.sqlite");

const houseAliases = {"ascendente": "casa1", "fundodoceu": "casa4", "ic": "casa4", "descendente": "casa7", "meiodoceu": "casa10", "mc": "casa10"};

function sqlInsertUpdate(data, output) {
  //console.log("Dump values within sqlinsertupdated ", data.values);

  sql.get(`SELECT * FROM ${data.table} WHERE userId="${data.user}"`).then(row => {
    let queryStr = '';
    if (!row) { // If row doesn't exist, add it to db
      /* INSERT SWITCH */
      switch(data.column) {
        case "astros": { // Full astros
          queryStr = "INSERT INTO astros (userId, sol, sol_retrogrado, sol_casa, lua, lua_retrogrado, lua_casa, mercurio, mercurio_retrogrado, mercurio_casa, venus, venus_retrogrado, venus_casa, marte, marte_retrogrado, marte_casa, jupiter, jupiter_retrogrado, jupiter_casa, saturno, saturno_retrogrado, saturno_casa, urano, urano_retrogrado, urano_casa, netuno, netuno_retrogrado, netuno_casa, plutao, plutao_retrogrado, plutao_casa, nodosul, nodosul_retrogrado, nodosul_casa, nodonorte, nodonorte_retrogrado, nodonorte_casa, quiron, quiron_retrogrado, quiron_casa, lilith, lilith_retrogrado, lilith_casa) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          sql.run(queryStr, [data.user, ...data.values])
          .then(() => {
            output(1);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
          break;
        }
        case "casas": { // Full casas
          queryStr = "INSERT INTO casas (userId, casa1, casa2, casa3, casa4, casa5, casa6, casa7, casa8, casa9, casa10, casa11, casa12) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          sql.run(queryStr, [data.user, ...data.values])
          .then(() => {
            output(1);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
          break;
        }
        default: {
          if (data.values.length === 3) { // Astro info
            queryStr = `INSERT INTO astros (userId, ${data.column}, ${data.column}_retrogrado, ${data.column}_casa) VALUES (?, ?, ?, ?)`;
          } else { // Other infos
            queryStr = `INSERT INTO ${data.table} (userId, ${data.column}) VALUES (?, ?)`;
          }
          sql.run(queryStr, [data.user, ...data.values])
          .then(() => {
            output(1);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
        }
      } // End switch
    } else { // Else update it
      /* UPDATE SWITCH */
      switch(data.column) {
        case "astros": { // Full astros
          queryStr = `UPDATE astros SET sol = ?, sol_retrogrado = ?, sol_casa = ?, lua = ?, lua_retrogrado = ?, lua_casa = ?, mercurio = ?, mercurio_retrogrado = ?, mercurio_casa = ?, venus = ?, venus_retrogrado = ?, venus_casa = ?, marte = ?, marte_retrogrado = ?, marte_casa = ?, jupiter = ?, jupiter_retrogrado = ?, jupiter_casa = ?, saturno = ?, saturno_retrogrado = ?, saturno_casa = ?, urano = ?, urano_retrogrado = ?, urano_casa = ?, netuno = ?, netuno_retrogrado = ?, netuno_casa = ?, plutao = ?, plutao_retrogrado = ?, plutao_casa = ?, nodosul = ?, nodosul_retrogrado = ?, nodosul_casa = ?, nodonorte = ?, nodonorte_retrogrado = ?, nodonorte_casa = ?, quiron = ?, quiron_retrogrado = ?, quiron_casa = ?, lilith = ?, lilith_retrogrado = ?, lilith_casa = ? WHERE userId = ?`;
          sql.run(queryStr, [...data.values, data.user])
          .then(() => {
            output(2);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
          break;
        }
        case "casas": { // Full casas
          queryStr = `UPDATE casas SET casa1 = ?, casa2 = ?, casa3 = ?, casa4 = ?, casa5 = ?, casa6 = ?, casa7 = ?, casa8 = ?, casa9 = ?, casa10 = ?, casa11 = ?, casa12 = ? WHERE userId = ?`;
          sql.run(queryStr, [...data.values, data.user])
          .then(() => {
            output(2);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
          break;
        }
        default: {
          if (data.values.length === 3) { // Astro info
            queryStr = `UPDATE astros SET ${data.column} = ?, ${data.column}_retrogrado = ?, ${data.column}_casa = ? WHERE userId = ?`;
          } else { // Other infos
            queryStr = `UPDATE ${data.table} SET ${data.column} = ? WHERE userId = ?`;
          }
          sql.run(queryStr, [...data.values, data.user])
          .then(() => {
            output(2);
          })
          .catch((err) => {
            output(3);
            console.log(`Erro! SQL: "${queryStr}"`,err);
          });
        }
      } // End switch
    } // End insert/update ifelse
  }).catch((err) => {
    console.error(err);
    output(3);
  });
}

function sqlSelect(data, output) {
  sql.get(`SELECT * FROM astros A LEFT JOIN casas C ON C.userId=A.userId WHERE A.userId ="${data.user}"`).then(row => {
    if (!row) { // If row (user) doesn't exist, inform them
      output(1);
    } else { // Else show map
      output(2, row);
    }
  }).catch((err) => {
    console.error(err);
    output(3);
  });
}

client.on("ready", () => {
  console.log(`Server ready... (v${Discord.version})`);
  // Prepare tables
  sql.run("CREATE TABLE IF NOT EXISTS astros (userId TEXT, sol TEXT, sol_retrogrado TEXT, sol_casa TEXT, lua TEXT, lua_retrogrado TEXT, lua_casa TEXT, mercurio TEXT, mercurio_retrogrado TEXT, mercurio_casa TEXT, venus TEXT, venus_retrogrado TEXT, venus_casa TEXT, marte TEXT, marte_retrogrado TEXT, marte_casa TEXT, jupiter TEXT, jupiter_retrogrado TEXT, jupiter_casa TEXT, saturno TEXT, saturno_retrogrado TEXT, saturno_casa TEXT, urano TEXT, urano_retrogrado TEXT, urano_casa TEXT, netuno TEXT, netuno_retrogrado TEXT, netuno_casa TEXT, plutao TEXT, plutao_retrogrado TEXT, plutao_casa TEXT, nodosul TEXT, nodosul_retrogrado TEXT, nodosul_casa TEXT, nodonorte TEXT, nodonorte_retrogrado TEXT, nodonorte_casa TEXT, quiron TEXT, quiron_retrogrado TEXT, quiron_casa TEXT, lilith TEXT, lilith_retrogrado TEXT, lilith_casa TEXT, astrolink TEXT)").then(() => {
    console.log("Table 'astros' verified!");
  })
  .catch((err) => {
    console.log("Erro! Ocorreu algum erro ao checar a tabela 'astros'!",err);
  });

  sql.run("CREATE TABLE IF NOT EXISTS casas (userId TEXT, casa1 TEXT, casa2 TEXT, casa3 TEXT, casa4 TEXT, casa5 TEXT, casa6 TEXT, casa7 TEXT, casa8 TEXT, casa9 TEXT, casa10 TEXT, casa11 TEXT, casa12 TEXT)").then(() => {
    console.log("Table 'casas' verified!");
  })
  .catch((err) => {
    console.log("Erro! Ocorreu algum erro ao checar a tabela 'casas'!",err);
  });
  client.user.setActivity('!helpme', { type: 'PLAYING' })
  .then(presence => console.log("Setting activity to !helpme..."))
  .catch(console.error);
});

client.on('error', error => {
  console.log(`Error found: "${error.name}"\nDescription: "${error.message}"`);
});

client.on("message", (message) => {
  // Exit and stop if it's not there
  if (!message.content.startsWith(config.prefix) || message.author.bot || message.channel.type === "dm") return;
  
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  if (command === "ola" || command === "oi") {
    message.reply("Oi, oi! :blush:");
  } else
  if (command === "tchau" || command === "adeus") {
    message.reply("Até breve!! :hugging:");
  } else
  if (command === "helpme") {
    message.reply("Oi! Te enviei os comandos por *Direct Message*. :blush:");
    message.author.send("", {embed: {
      title: "**Comandos - Vênus Bot**",
      description: `Estes são os comandos que eu tenho disponíveis. ${misc.randomEmotion("success")}`,
      thumbnail: {url: "https://i.imgur.com/VQiOeTk.png"},
      fields: [{
        name: "!astrolink [link do perfil]",
        value: "Salva seu link de perfil do Astrolink. Deve conter apenas números!\n**Ex.:** !astrolink 444000888555\n⁪"
      },
      {
        name: "!setastros [sol] [casa] [lua] [casa] [mercurio] [casa] [venus] [casa] [marte] [casa] [jupiter] [casa] [saturno] [casa] [urano] [casa] [netuno] [casa] [plutao] [casa] [nodo sul] [casa] [nodo norte] [casa] [quiron] [casa] [lilith] [casa]",
        value: "Cadastra seus astros (planetas). Deve conter 14 planetas seguidos por suas casa; se o planeta for *retrógrado* inclua o sufixo -R no *nome do signo* (ex. Áries-R)!\n**Ex.:** !setastros Peixes 10 Gêmeos 1 Áries 11 Aquário 10 Aquário 4 Virgem-R 4 Aquário 9 Capricórnio 8 Capricórnio 8 Escorpião-R 7 Capricórnio 8 Capricórnio 8 Leão-R 3 Aquário 9\n⁪"
      },
      {
        name: "!setcasas [casa1] [casa2] [casa3] [casa4] [casa5] [casa6] [casa7] [casa8] [casa9] [casa10] [casa11] [casa12]",
        value: "Cadastra suas casas astrológicas. Deve conter 12 casas, cada qual com um número entre 1 e 12!\n**Ex.:** !setcasas Touro Gêmeos Câncer Leão Virgem Libra Escorpião Sagitário Capricórnio Aquário Peixes Áries\n⁪"
      },
      {
        name: "!set<astro> [signo] [casa]",
        value: "Cadastra apenas um astro, especificando seu signo e casa respectivamente. O astro pode ser um dos 14 planetas (ex. sol, lua, venus, nodonorte, nodosul, etc)!\n⁪**Exs.:**\n!setplutao Libra 12\n!setsol Capricórnio 2\n!setnodosul Aquário 7\n"
      },
      {
        name: "!set<casa> [signo]",
        value: "Cadastra apenas uma casa, especificando seu signo. A casa pode ser um das 12 disponíveis (ex. casa1, casa2, casa3, etc)!\n**Obs.:** Existem 4 casas especiais que possuem outro apelido na astrologia moderna: ascendente (casa1), fundo do céu (casa4), descendente (cssa7) e meio do céu (casa10). Todas podem ser utilizadas no lugar do número da casa, mas representam a mesma coisa.\n\n⁪**Exs.:**\n!setcasa1 Câncer\n!setmeiodoceu Touro\n!setnodosul Aquário\n⁪"
      },
      {
        name: "!showastros [membro/opcional]",
        value: "Exibe seus astros, ou os astros de outro membro caso ele tenha sido mencionado!\n**Ex.:** !showastros @Fulano\n⁪"
      },
      {
        name: "!showcasas [membro/opcional]",
        value: "Exibe suas casas, ou as casas de outro membro caso ele tenha sido mencionado!\n**Ex.:** !showcasas @Fulano\n⁪"
      },
      {
        name: "!showmapa [membro/opcional]",
        value: "Exibe seu mapa astral completo (astros e casas), ou o mapa astral de outro membro caso ele tenha sido mencionado!\n**Ex.:** !showmapa @Fulano\n"
      }]
    }});
  } else 
  if (command === "astrolink") {
      // Validations
      if (!args.length) {
        message.reply(`Dã! Você precisa informar seu link, né. ${misc.randomEmotion("error-mocking")}`);
        return;
      }
      if (args[0].match(/[^0-9]/g)!==null) {
        message.reply(`Dã! Link deve conter apenas os números do seu perfil. ${misc.randomEmotion("error-mocking")}`);
        return;
      }

      // Query
      sqlInsertUpdate({user: message.author.id, table: 'astros', column: 'astrolink', values: args}, (status) => {
        switch(status) {
          case 1:
            message.reply(`Astrolink cadastrado com sucesso! ${misc.randomEmotion("success")}`);
            break;
          case 2:
            message.reply(`Astrolink atualizado com sucesso! ${misc.randomEmotion("success")}`);
            break;
          case 3:
            message.reply(`Vixe! Houve algum erro ao criar/editar seu link... ${misc.randomEmotion("error")}`);
            break;
        }
      });
  } else
  if (command.substr(0, 3) === "set") {
    // Only allow set commands in the specific channel to avoid clutter in various channels (admin channels are whitelisted by default)
    if (!(config.adminchannels.concat(config.channel)).includes(message.channel.name)) {
      let channelId = client.channels.find("name", config.channel).id;
      message.reply(`Por favor, configure seu mapa astral na sala <#${channelId}>; assim a gente evita de spammar as salas de bate-papo com esses comandos! :wink:`);
      return;
    }
    
    // Identify command
    let column     = command.slice(3),
        table      = "",
        targetUser = message.author,
        valuesQty  = 0,
        values     = args;
    
    // Check for extra arguments to catch length, and to catch mentioned user if available
    if (args.length>0) {

      // Check for mentioned user
      if (message.mentions.members.first()) {
        if (!(message.member.roles.find("name", "Administrador")||message.member.roles.find("name", "Moderador"))) {
          message.reply("Você não tem permissão para fazer isso! :wink:");
          return;
        }

        targetUser = message.mentions.members.first().user; // Attempt to change someone else's data
        values.shift(); // Pop first argument from list, i.e. the mentioned user (hopefully)
      }

      // Correct quantity
      valuesQty = values.length;
    } // Don't message anything, switch statement will gracefully message the respective error
    
    // Resolve house aliases, if suitable
    column = (houseAliases[column]||column);

    switch(column) {
      case "astros": { // Full astros
        // Validations

        // Minimum arguments = 28
        if (valuesQty!=28) {
          message.reply(`Você precisa informar os **14 planetas**! ${misc.randomEmotion("error")}\nÉ assim ó:\n\n\`\`!setastros <Sol> <Casa> <Lua> <Casa> <Mercúrio> <Casa> <Vênus> <Casa> <Marte> <Casa> <Júpiter> <Casa> <Saturno> <Casa> <Urano> <Casa> <Netuno> <Casa> <Plutão> <Casa> <Nodo Sul> <Casa> <Nodo Norte> <Casa> <Quiron> <Casa> <Lilith> <Casa>\`\``);
          return;
        }

        // Valid signs, and valid houses
        for (let i=0;i<valuesQty;i++) {
          if (i%2===0&&!misc.validateSign(values[i])) { // 12 signs
            message.reply(`Dã! Não existe o signo "${values[i].replace('-R','')}". ${misc.randomEmotion("error-mocking")}`);
            return;
          } else
          if (i%2===1&&!misc.validateHouse(values[i])) { // 12 houses
            message.reply(`Dã! Não existe a casa "${values[i]}". ${misc.randomEmotion("error-mocking")}`);
            return;
          }
        }

        table = "astros"; // Table name
        values = misc.transformRetrogradation(values); // Split retrogradation
        break;
      }
      case "sol": case "lua": case "mercurio": case "venus": case "marte": case "jupiter": case "saturno": case "urano": case "netuno": case "plutao": case "nodosul": case "nodonorte": case "quiron": case "lilith": {
        // Validations
        if (valuesQty !== 2) {
          message.reply(`Dã! Você precisa informar seu **signo** e **casa**! ${misc.randomEmotion("error-mocking")}\nÉ assim ó:\n\`\`\`\!set${column} <Signo> <Casa>\`\`\``);
          return;
        }

        // Valid sign
        if (!misc.validateSign(values[0])) {
          message.reply(`Dã! Não existe o signo "${values[0].replace('-R','')}". ${misc.randomEmotion("error-mocking")}`);
          return;
        }

        // Valid house
        if (!misc.validateHouse(values[1])) {
          message.reply(`Dã! Não existe a casa "${values[1]}". ${misc.randomEmotion("error-mocking")}`);
          return;
        }

        table = "astros"; // Table name
        values = misc.transformRetrogradation(values); // Split retrogradation
        break;
      }
      case "casas": { // Full casas
        // Validations

        // Minimum arguments = 12
        if (valuesQty!=12) {
          message.reply(`Você precisa informar as **12 casas**! ${misc.randomEmotion("error")}\nÉ assim ó:\n\n\`\`!setcasas <Casa1> <Casa2> <Casa3> <Casa4> <Casa5> <Casa6> <Casa7> <Casa8> <Casa9> <Casa10> <Casa11> <Casa12>\`\``);
          return;
        }

        // Valid signs
        for (let i=0;i<valuesQty;i++) {
          if (!misc.validateSign(values[i])) { // 12 signs
            message.reply(`Dã! Não existe o signo "${values[i]}".  ${misc.randomEmotion("error-mocking")}`);
            return;
          }
        }

        table = "casas"; // Table name
        break;
      }
      case "casa1": case "casa2": case "casa3": case "casa4": case "casa5": case "casa6": case "casa7": case "casa8": case "casa9": case "casa10": case "casa11": case "casa12": {
        // Validations
        if (valuesQty !== 1) {
          message.reply(`Dã! Você precisa informar seu **signo**! ${misc.randomEmotion("error-mocking")}\nÉ assim ó:\n\`\`\`\!set${column} <Signo>\`\`\``);
          return;
        }

        // Valid sign
        if (!misc.validateSign(values[0])) {
          message.reply(`Dã! Não existe o signo "${values[0]}". ${misc.randomEmotion("error-mocking")}`);
          return;
        }

        table = "casas"; // Table name
        break;
      }
      default: {
        message.reply(`Vixe! Comando "${command}" desconhecido. ${misc.randomEmotion("error")}`);
        return;
      }
    }

    // Perform query
    sqlInsertUpdate({user: targetUser.id, table: table, column: column, values: values.map((val) => {
      if (typeof val === "boolean") { // Retrogradation flag
        return val;
      } else if (val.match(/^[0-9]/)!==null) { // House number
        return misc.padNumLeft(val);
      } else {
        return misc.capitalizeSign(val); // Sign name
      }
    })}, (status) => {
      switch(status) {
        case 1:
          message.reply(`Dados cadastrados com sucesso! ${misc.randomEmotion("success")}`);
          break;
        case 2:
          message.reply(`Dados atualizados com sucesso! ${misc.randomEmotion("success")}`);
          break;
        case 3:
          message.reply(`Vixe! Houve algum erro ao criar/editar ${targetUser.id===message.author.id?"suas":"as"} informações... ${misc.randomEmotion("error")}`);
          break;
      }
    });
  } else
  if (command === "showmapa" || command === "showastros" || command === "showcasas") {
    let targetUser = args.length>0 ? message.mentions.members.first().user : message.author;
    let mapStyle = command.replace("show","");

    sqlSelect({user: targetUser.id, table: mapStyle}, (status, mapData) => {
      switch(status) {
        case 1:
          message.reply(`Vixe! Mapa inexistente... ${misc.randomEmotion("error")}`);
          break;
        case 2:
          let link = (mapData.astrolink ? `perfil.php?id=${mapData.astrolink}` : "");
          let styledDescription = `*[Perfil Astrolink${link==""?" (?)":""}](https://www.astrolink.com.br/${link})*\n\n\`\`\`autohotkey\n`;

          if (mapStyle === "mapa"||mapStyle === "astros") {
            styledDescription += `%╔══════════════════════════════╗%
%║██████████ PLANETAS ██████████║%
%╚══════════════════════════════╝%
`;
            styledDescription += "\n☉ Sol:";
            if (mapData.sol) styledDescription += ` ${misc.zodiacSymbol(mapData.sol)}${mapData.sol_retrogrado==1?" (R.)":""}`;
            if (mapData.sol_casa) styledDescription += ` na Casa ${mapData.sol_casa}${misc.planetaryDignity("sol",mapData.sol)}`;

            styledDescription += "\n☽ Lua:";
            if (mapData.lua) styledDescription += ` ${misc.zodiacSymbol(mapData.lua)}${mapData.lua_retrogrado==1?" (R.)":""}`;
            if (mapData.lua_casa) styledDescription += ` na Casa ${mapData.lua_casa}${misc.planetaryDignity("lua",mapData.lua)}`;

            styledDescription += "\n☿ Mercúrio:";
            if (mapData.mercurio) styledDescription += ` ${misc.zodiacSymbol(mapData.mercurio)}${mapData.mercurio_retrogrado==1?" (R.)":""}`;
            if (mapData.mercurio_casa) styledDescription += ` na Casa ${mapData.mercurio_casa}${misc.planetaryDignity("mercurio",mapData.mercurio)}`;

            styledDescription += "\n♀ Vênus:";
            if (mapData.venus) styledDescription += ` ${misc.zodiacSymbol(mapData.venus)}${mapData.venus_retrogrado==1?" (R.)":""}`;
            if (mapData.venus_casa) styledDescription += ` na Casa ${mapData.venus_casa}${misc.planetaryDignity("venus",mapData.venus)}`;

            styledDescription += "\n♂ Marte:";
            if (mapData.marte) styledDescription += ` ${misc.zodiacSymbol(mapData.marte)}${mapData.marte_retrogrado==1?" (R.)":""}`;
            if (mapData.marte_casa) styledDescription += ` na Casa ${mapData.marte_casa}${misc.planetaryDignity("marte",mapData.marte)}`;

            styledDescription += "\n♃ Júpiter:";
            if (mapData.jupiter) styledDescription += ` ${misc.zodiacSymbol(mapData.jupiter)}${mapData.jupiter_retrogrado==1?" (R.)":""}`;
            if (mapData.jupiter_casa) styledDescription += ` na Casa ${mapData.jupiter_casa}${misc.planetaryDignity("jupiter",mapData.jupiter)}`;

            styledDescription += "\n♄ Saturno:";
            if (mapData.saturno) styledDescription += ` ${misc.zodiacSymbol(mapData.saturno)}${mapData.saturno_retrogrado==1?" (R.)":""}`;
            if (mapData.saturno_casa) styledDescription += ` na Casa ${mapData.saturno_casa}${misc.planetaryDignity("saturno",mapData.saturno)}`;

            styledDescription += "\n♅ Urano:";
            if (mapData.urano) styledDescription += ` ${misc.zodiacSymbol(mapData.urano)}${mapData.urano_retrogrado==1?" (R.)":""}`;
            if (mapData.urano_casa) styledDescription += ` na Casa ${mapData.urano_casa}`;

            styledDescription += "\n♆ Netuno:";
            if (mapData.netuno) styledDescription += ` ${misc.zodiacSymbol(mapData.netuno)}${mapData.netuno_retrogrado==1?" (R.)":""}`;
            if (mapData.netuno_casa) styledDescription += ` na Casa ${mapData.netuno_casa}`;

            styledDescription += "\n♇ Plutão:";
            if (mapData.plutao) styledDescription += ` ${misc.zodiacSymbol(mapData.plutao)}${mapData.plutao_retrogrado==1?" (R.)":""}`;
            if (mapData.plutao_casa) styledDescription += ` na Casa ${mapData.plutao_casa}`;

            styledDescription += "\n☊ Nodo Norte:";
            if (mapData.nodonorte) styledDescription += ` ${misc.zodiacSymbol(mapData.nodonorte)}${mapData.nodonorte_retrogrado==1?" (R.)":""}`;
            if (mapData.nodonorte_casa) styledDescription += ` na Casa ${mapData.nodonorte_casa}`;

            styledDescription += "\n☋ Nodo Sul:";
            if (mapData.nodosul) styledDescription += ` ${misc.zodiacSymbol(mapData.nodosul)}${mapData.nodosul_retrogrado==1?" (R.)":""}`;
            if (mapData.nodosul_casa) styledDescription += ` na Casa ${mapData.nodosul_casa}`;

            styledDescription += "\n⚷ Quiron:";
            if (mapData.quiron) styledDescription += ` ${misc.zodiacSymbol(mapData.quiron)}${mapData.quiron_retrogrado==1?" (R.)":""}`;
            if (mapData.quiron_casa) styledDescription += ` na Casa ${mapData.quiron_casa}`;

            styledDescription += "\n⚸ Lilith:";
            if (mapData.lilith) styledDescription += ` ${misc.zodiacSymbol(mapData.lilith)}${mapData.lilith_retrogrado==1?" (R.)":""}`;
            if (mapData.lilith_casa) styledDescription += ` na Casa ${mapData.lilith_casa}`;
            styledDescription += "\n";
          }

          if (mapStyle === "mapa"||mapStyle === "casas") {
            styledDescription += `
%╔═══════════════════════════╗%
%║██████████ CASAS ██████████║%
%╚═══════════════════════════╝%
`;
            styledDescription += "\nCasa 1 (Ascendente):";
            if (mapData.casa1) styledDescription += ` ${misc.zodiacSymbol(mapData.casa1)}`;

            styledDescription += "\nCasa 2:";
            if (mapData.casa2) styledDescription += ` ${misc.zodiacSymbol(mapData.casa2)}`;

            styledDescription += "\nCasa 3:";
            if (mapData.casa3) styledDescription += ` ${misc.zodiacSymbol(mapData.casa3)}`;

            styledDescription += "\nCasa 4 (Fundo do Céu):";
            if (mapData.casa4) styledDescription += ` ${misc.zodiacSymbol(mapData.casa4)}`;

            styledDescription += "\nCasa 5:";
            if (mapData.casa5) styledDescription += ` ${misc.zodiacSymbol(mapData.casa5)}`;

            styledDescription += "\nCasa 6:";
            if (mapData.casa6) styledDescription += ` ${misc.zodiacSymbol(mapData.casa6)}`;

            styledDescription += "\nCasa 7 (Descendente):";
            if (mapData.casa7) styledDescription += ` ${misc.zodiacSymbol(mapData.casa7)}`;

            styledDescription += "\nCasa 8:";
            if (mapData.casa8) styledDescription += ` ${misc.zodiacSymbol(mapData.casa8)}`;

            styledDescription += "\nCasa 9:";
            if (mapData.casa9) styledDescription += ` ${misc.zodiacSymbol(mapData.casa9)}`;

            styledDescription += "\nCasa 10 (Meio do Céu):";
            if (mapData.casa10) styledDescription += ` ${misc.zodiacSymbol(mapData.casa10)}`;

            styledDescription += "\nCasa 11:";
            if (mapData.casa11) styledDescription += ` ${misc.zodiacSymbol(mapData.casa11)}`;

            styledDescription += "\nCasa 12:";
            if (mapData.casa12) styledDescription += ` ${misc.zodiacSymbol(mapData.casa12)}`;
          }

          styledDescription += "```";

          message.channel.send("", {embed: {
            author: {
              name:  targetUser.username,
              icon_url: targetUser.avatarURL
            },
            title: "**Mapa Astral Personalizado**",
            description: styledDescription}});
          break;
        case 3:
          message.reply(`Vixe! Houve algum erro ao exibir seu mapa astral... ${misc.randomEmotion("error")}`);
          break;
      }
    });
  }
  
});

client.login(config.token);