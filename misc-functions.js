const errorEmotions = [":thinking:", ":nerd:", ":relieved:", ":confused:"];
const successEmotions = [":blush:", ":wink:", ":slight_smile:", ":smile:"];
const mockingEmotions = [":relieved:", ":stuck_out_tongue_winking_eye:", ":sweat_smile:", ":grimacing:"];

/* Pick a random emotion based on type */
module.exports.randomEmotion = (type) => {
  switch(type) {
    case "error":
      return errorEmotions[Math.floor(Math.random() * 4)];
    case "success":
      return successEmotions[Math.floor(Math.random() * 4)];
    case "error-mocking":
      return mockingEmotions[Math.floor(Math.random() * 4)];
  }
}

/* Pads two zeroes to the number. */
var padNumLeft = module.exports.padNumLeft = (val) => {
  return `${parseInt(val)}`.padStart(2, "0");
}

/* Validates a sign, removing the optional '-R' string (for retrogradation). */
module.exports.validateSign = (sign) => {
  sign = sign.replace(/\-R/i, '');
  return sign.match(/^(Aquário|Aquario|Peixes|Áries|Aries|Touro|Gêmeos|Gemeos|Câncer|Cancer|Leão|Leao|Virgem|Libra|Escorpião|Escorpiao|Sagitário|Sagitario|Capricórnio|Capricornio)$/i)!=null;
}

/* Validates a house; must be a number between 1-12. */
module.exports.validateHouse = (house) => {
  return (!isNaN(house)&&parseInt(house)>=1&&parseInt(house)<=12);
}

/* Converts a list of [<Sign> <House>] including the optional retrogradation flag. */
module.exports.transformRetrogradation = (signs) => {
  // ["Peixes", "10", "Aries-R", "6"] => ["Peixes", false, "10", "Aries", true, "06"]
  return signs.reduce(
    function(acc, val) {
      if (val.match(/[A-Z]/gi)!==null) { // Contains letter, therefore a sign
        let currSign = val.split('-');
        return acc.concat([currSign[0], (currSign[1]?true:false)]); // 0 - Sign, 1 - ("R"||undefined) = (true||false)
      } else { // Doesn't contain letter, therefore a house
        return acc.concat(padNumLeft(val));
      }
    },
    []
  );
}

/* Capitalizes the sign name. */
module.exports.capitalizeSign = (sign) => {
  switch(sign.toLowerCase()) {
    case "aquário": case "aquario":
      return "Aquário";
    case "áries": case "aries":
      return "Áries";
    case "gêmeos": case "gemeos":
      return "Gêmeos";
    case "câncer": case "cancer":
      return "Câncer";
    case "leão": case "leao":
      return "Leão";
    case "escorpião": case "escorpiao":
      return "Escorpião";
    case "sagitário": case "sagitario":
      return "Sagitário";
    case "capricórnio": case "capricornio":
      return "Capricórnio";
    default:
      return (sign.charAt(0).toUpperCase() + sign.slice(1));
  }
}

/* Returns zodiac symbol for a sign. */
module.exports.zodiacSymbol = (sign) => {
  switch(sign) {
    case "Aquário":
      return "♒ Aquário";
    case "Áries":
      return "♈ Áries";
    case "Câncer":
      return "♋ Câncer";
    case "Capricórnio":
      return "♑ Capricórnio";
    case "Escorpião":
      return "♏ Escorpião";
    case "Gêmeos":
      return "♊ Gêmeos";
    case "Leão":
      return "♌ Leão";
    case "Libra":
      return "♎ Libra";
    case "Peixes":
      return "♓ Peixes";
    case "Sagitário":
      return "♐ Sagitário";
    case "Touro":
      return "♉ Touro";
    case "Virgem":
      return "♍ Virgem";
    
  }
}

/* Returns planetary dignity if it applies to current sign. */
module.exports.planetaryDignity = (planet, sign) => {
  switch(planet) {
    case "sol": {
      if (sign==="Leão") return " ★ Domicílio!";
      if (sign==="Aquário") return " ≠ Exílio!";
      if (sign==="Áries") return " ▲ Exaltação!";
      if (sign==="Libra") return " ▼ Queda!";
      break;
    }
    case "lua": {
      if (sign==="Câncer") return " ★ Domicílio!";
      if (sign==="Capricórnio") return " ≠ Exílio!";
      if (sign==="Touro") return " ▲ Exaltação!";
      if (sign==="Escorpião") return " ▼ Queda!";
      break;
    }
    case "mercurio": {
      if (sign==="Gêmeos"||sign==="Virgem") return " ★ Domicílio!";
      if (sign==="Sagitário"||sign==="Peixes") return " ≠ Exílio!";
      if (sign==="Aquário") return " ▲ Exaltação!";
      if (sign==="Leão") return " ▼ Queda!";
      break;
    }
    case "venus": {
      if (sign==="Touro"||sign==="Libra") return " ★ Domicílio!";
      if (sign==="Áries"||sign==="Escorpião") return " ≠ Exílio!";
      if (sign==="Peixes") return " ▲ Exaltação!";
      if (sign==="Virgem") return " ▼ Queda!";
      break;
    }
    case "marte": {
      if (sign==="Áries"||sign==="Escorpião") return " ★ Domicílio!";
      if (sign==="Touro"||sign==="Libra") return " ≠ Exílio!";
      if (sign==="Capricórnio") return " ▲ Exaltação!";
      if (sign==="Câncer") return " ▼ Queda!";
      break;
    }
    case "jupiter": {
      if (sign==="Sagitário"||sign==="Peixes") return " ★ Domicílio!";
      if (sign==="Gêmeos"||sign==="Virgem") return " ≠ Exílio!";
      if (sign==="Câncer") return " ▲ Exaltação!";
      if (sign==="Capricórnio") return " ▼ Queda!";
      break;
    }
    case "saturno": {
      if (sign==="Capricórnio"||sign==="Aquário") return " ★ Domicílio!";
      if (sign==="Câncer"||sign==="Leão") return " ≠ Exílio!";
      if (sign==="Libra") return " ▲ Exaltação!";
      if (sign==="Áries") return " ▼ Queda!";
      break;
    }
    /*case "urano": {
      
      break;
    }
    case "netuno": {
      
      break;
    }
    case "plutao": {
      
      break;
    }
    case "nodonorte": {
      
      break;
    }
    case "nodosul": {
      
      break;
    }
    case "quiron": {
      
      break;
    }
    case "lilith": {
      
      break;
    }*/
  }

  return "";
}