# Venus Bot
Simple Astrology Bot for Discord.

### Running

Checkout this repo, install dependencies, then start server with the following:

```
> git clone git@bitbucket.org:paulofilho92/venus-bot.git
> cd venus-bot
> npm install
> node app
```